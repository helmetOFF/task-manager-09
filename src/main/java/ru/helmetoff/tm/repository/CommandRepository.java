package ru.helmetoff.tm.repository;

import ru.helmetoff.tm.api.ICommandRepository;
import ru.helmetoff.tm.constant.IArgumentConst;
import ru.helmetoff.tm.constant.ITerminalConst;
import ru.helmetoff.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            ITerminalConst.HELP, IArgumentConst.HELP, ITerminalConst.HELP_DESCRIPTION
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            ITerminalConst.ABOUT, IArgumentConst.ABOUT, ITerminalConst.ABOUT_DESCRIPTION
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            ITerminalConst.VERSION, IArgumentConst.VERSION, ITerminalConst.VERSION_DESCRIPTION
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            ITerminalConst.INFO, IArgumentConst.INFO, ITerminalConst.INFO_DESCRIPTION
    );

    public static final TerminalCommand SHOW_COMMANDS = new TerminalCommand(
            ITerminalConst.COMMANDS, IArgumentConst.COMMANDS, ITerminalConst.COMMANDS_DESCRIPTION
    );

    public static final TerminalCommand SHOW_ARGUMENTS = new TerminalCommand(
            ITerminalConst.ARGUMENTS, IArgumentConst.ARGUMENTS, ITerminalConst.ARGUMENTS_DESCRIPTION
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            ITerminalConst.EXIT, null, ITerminalConst.EXIT_DESCRIPTION
    );

    private static TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[] {
            HELP, ABOUT, VERSION, INFO, SHOW_COMMANDS, SHOW_ARGUMENTS, EXIT
    };

    public String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    public String[] ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArguments(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArguments() {
        return ARGUMENTS;
    }

}
